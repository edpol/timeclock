<?php

class new_id {

	public static function generate() {
		srand(self::make_seed()); // - Seed the random number generator
		$id = self::random_string(11);
		$id[] = self::check_digit($id);
		$new_id = implode("", $id);
		return $new_id;
	}

	// seed with microseconds
	private static function make_seed() {
		list($usec, $sec) = explode(' ', microtime());
		return $sec + $usec * 1000000;
	}

	private static function random_string($cnt) {
		$id = array();
		for ($i=0; $i<11; $i++) {
		    $randval = rand(0, 9);
    		$id[] = $randval;
		}
		return $id;
	}

	private static function check_digit($array_of_numbers) {

		$l =sizeof($array_of_numbers);

		if ($l % 2 == 0) {
			$F16 = 0;
		} else {
			$l--;
			$F16 = $array_of_numbers[$l]*3;
		}

		for ($i=0; $i<$l; $i+=2) {
			$F16 += $array_of_numbers[$i]*3 + $array_of_numbers[$i+1];
		}
		$F16 = 140 - $F16;

		$G16=-1;
		for ($i=100; $i>29; $i-=10) {
			if ($F16>($i-1)) {
				$G16=$F16-$i;
				break;
			} 
		} 
		if ($G16<0) $G16=$F16;

		if ($F16>19) $H16=$F16-20; 
			else if ($H16=$F16>9) $H16=$F16-10; 
				else $H16=$F16;

		return ($G16>$H16) ? $I16=$H16 : $I16=$G16;
	}

}

?>