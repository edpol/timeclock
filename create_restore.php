<?php

require_once ("include/connect.php");

$line = array();
$line[] = ":: to restore from a backup (it converts from UTC to your time zone)";
$line[] = "mysql -u " . DB_USER . " -p " . DB_NAME . " < timeclock.sql ";

$handle = fopen("restore.bat", "w");

$l = count($line);
for ($i=0; $i<$l; $i++) {
	fwrite($handle, $line[$i] . PHP_EOL);
}
fclose($handle); 
?>