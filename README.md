# README #

* VERY simple timeclock routine.
* Version 1

### How do I get set up? ###

* Summary of set up
```
create database mdr_clock;
use mdr_clock;
GRANT ALL PRIVILEGES ON mdr_clock.* TO 'chronos'@'localhost'       IDENTIFIED BY 'madmax';
```
enter the password in the file include/connect.php
```
### Table 1
/*
 *    This table contains all of the employee information
 *    BARCODE and LNAME are the only required fields
 */
drop table if exists employee;
create table employee(
  employeeid  integer unsigned  not null   auto_increment,
  is_active   boolean           not null   default 1,
  barcode     char(12)          not null,
  fname       varchar(30),
  lname       varchar(30),
  email       varchar(50),
  add1        varchar(30),
  add2        varchar(30),
  city        varchar(30),
  st          varchar(2),
  zip         varchar(10),
  phone       varchar(10),
  social      varchar(10),
  hire_date   timestamp         not null   default now(),
  primary key(employeeid),
  unique key (barcode)
) ENGINE=Innodb DEFAULT CHARSET=utf8;
```

```
### Table 2
/*
 *    This table contains all of the timestamps.
 *    This is populated by program unless you have some history to add.
 */
drop table if exists timeclock;
create table timeclock(
  idx           bigint  unsigned  not null  auto_increment,
  employeeid    integer unsigned  not null,
  punch         timestamp         not null  default now(),
  primary key(idx),
  index(employeeid),
  index(punch),
  CONSTRAINT FK_id FOREIGN KEY (employeeid) REFERENCES employee(employeeid) 
  ON UPDATE CASCADE
  ON DELETE CASCADE;
) ENGINE=Innodb DEFAULT CHARSET=utf8;
```

```
### Table 3
/*
 *    This is for groups if you want to divide up the payroll
 *    MUST HAVE at least one.
 */
drop table if exists groups;
create table groups(
  idx           integer unsigned     not null     auto_increment,
  groupname     varchar(30),
  primary key(idx),
  index(groupname)
) ENGINE=Innodb DEFAULT CHARSET=utf8;
```

enter employees in table with whatever barcode you wish.
They are now ready to use timeclock.

The username and passwords are presently stored in another database.
I have several utilities that access the same user table.
I will be changing this to stand alone soon.

you can email me at edpol03@gmail.com
